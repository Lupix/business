util.AddNetworkString("lpx_create_business_prompt")
util.AddNetworkString("lpx_create_business")
util.AddNetworkString("lpx_business_change_name")
util.AddNetworkString("lpx_business_change_cut")

Businesses = Businesses or {} -- create a table with all businesses

include("lpx_business_db.lua")

concommand.Add("lpx_biz_name", function(ply, cmd, args)
	if #args < 1 or #args > 1 then
		ply:PrintMessage(HUD_PRINTCONSOLE, "[Business] The amount of arguments does not match ( lpx_business_name 'business name' )")
		return
	end

	if ply:Team() == TEAM_BUSINESSMAN then
		local biz = GetBusinessByOwner(ply)
		local oldName = biz["Name"]
		biz["Name"] = tostring(args[1])
		if not SaveBusiness(biz) then
			ply:PrintMessage(HUD_PRINTCONSOLE, "[Business] That business name has already been chosen, please choose another one.")
			biz["Name"] = oldName
		else 
			for i=1, #(biz["Employees"]) do
				net.Start("lpx_business_change_name")
				net.WriteString(biz["Name"])
				net.WriteString(oldName)
				net.Send(biz["Employees"][i])
			end
			net.Start("lpx_business_change_name")
			net.WriteString(biz["Name"])
			net.WriteString(oldName)
			net.Send(ply)
		end
	else 
		ply:PrintMessage(HUD_PRINTCONSOLE, "[Business] You must be a Business Manager to use this command.")
	end
end
)

concommand.Add("lpx_biz_cut", function(ply, cmd, args)
	if #args < 1 or #args > 1 then
		ply:PrintMessage(HUD_PRINTCONSOLE, "[Business] The amount of arguments does not match ( lpx_biz_cut amount )")
		return
	end

	if ply:Team() == TEAM_BUSINESSMAN then
		if isnumber(args[1]) and args[1] >= 10 and args[1] <= 40 then
			local biz = GetBusinessByOwner(ply)
			local oldCut = biz["MgrCut"]
			biz["MgrCut"] = args[1]
			SaveBusiness(biz)

			for i=1, #(biz["Employees"]) do
				net.Start("lpx_business_change_cut")
				net.WriteString(biz["Name"])
				net.WriteInt(oldCut, 32)
				net.WriteInt(biz["MgrCut"], 32)
				net.Send(biz["Employees"][i])
			end
			net.Start("lpx_business_change_cut")
			net.WriteString(biz["Name"])
			net.WriteInt(oldCut, 32)
			net.WriteInt(biz["MgrCut"], 32)
			net.Send(ply)
		else
			ply:PrintMessage(HUD_PRINTCONSOLE, "[Business] The cut must be a number between (and including) 10 and 40!")
		end
	else 
		ply:PrintMessage(HUD_PRINTCONSOLE, "[Business] You must be a Business Manager to use this command.")
	end
end
)

net.Receive("lpx_create_business", function(len, ply) 
	local bizName = net.ReadString()
	local bizMgrCut = net.ReadInt(32)
	local Business = {Name = bizName, MgrCut = bizMgrCut, Owner = ply, Employees = {}}
	if not SaveBusiness(Business) then 
		ply:PrintMessage(HUD_PRINTTALK, "The Business name '".. Business["Name"] .."' has already been taken. Please choose another one.")
		net.Start("lpx_create_business_prompt")
		net.Send(ply)
	end
	table.insert(Businesses, Business)
	print("Created Business ".. bizName .." | Manager Cut: ".. bizMgrCut .."% | Owner: ".. ply:Nick())
end)

local function RemoveBusiness(biz)
	if biz then
		local i
		for i=1, #(biz["Employees"]) do
			biz["Employees"][i].AssignedComputer:SetOccupied(false)
			biz["Employees"][i].AssignedComputer = nil
			if biz["Employees"][i].UsingComputer then
				biz["Employees"][i].UsingComputer = false
				biz["Employees"][i]:Freeze(false)
				biz["Employees"][i]:GodDisable()
			end
			biz["Employees"][i]:PrintMessage(HUD_PRINTTALK, "The Company Manager has gone on vacation, you can join another company or wait for them to come back.")
		end

		for k, v in pairs(ents.FindByClass("lpx_computer")) do
			if v:GetManager() == biz["Owner"] then
				v:Remove()
			end
		end
		table.remove(Businesses, i)
	end
end

function GetBusinessByOwner(owner)
	for i=1, #Businesses do
		if Businesses[i]["Owner"] == owner then return Businesses[i] end
	end
	return false
end

hook.Add("OnPlayerChangedTeam", "PromptBizCreate", function(ply, oldTeam, newTeam)
	if oldTeam == TEAM_BUSINESSMAN then
		local biz = GetBusinessByOwner(ply)
		RemoveBusiness(biz)
		if ply.AssignedComputer then ply.AssignedComputer:SetOccupied(false) end
	end

	if(newTeam == TEAM_BUSINESSMAN) then

		ply:PrintMessage(HUD_PRINTTALK, "This server is running Business BETA by Lupix. Please report all bugs to the forums.")

		if not LoadBusinessByOwner(ply) then
			net.Start("lpx_create_business_prompt")
			net.Send(ply)
		else 
			ply:PrintMessage(HUD_PRINTTALK, "Welcome back to " .. GetBusinessByOwner(ply)["Name"] .. ", ".. ply:Nick() ..".")
		end
	end
end
)

hook.Add("PlayerDisconnected","RemoveBusiness", function(ply)
	local biz = GetBusinessByOwner(ply)
	if biz then
		RemoveBusiness(biz)
	end
	if ply.AssignedComputer then ply.AssignedComputer:SetOccupied(false) end
end
)

hook.Add("Initialize", "InitAddon", function() TableInit() end)
