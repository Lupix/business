AddCSLuaFile()

net.Receive("lpx_create_business_prompt", function(len, ply)
	local DCreateBtn
	local ManagerCutInfo
	local ManagerCutLbl
	local ManagerCut
	local DBizLbl
	local DBizTxt
	local DCreateBiz

	DCreateBiz = vgui.Create('DFrame')
	DCreateBiz:SetSize(ScrW() * 0.20, ScrH() * 0.15)
	DCreateBiz:Center()
	DCreateBiz:SetTitle('Create Business')
	DCreateBiz:SetSizable(true)
	DCreateBiz:SetDeleteOnClose(false)
	DCreateBiz:MakePopup()
	DCreateBiz:ShowCloseButton(false)

	DBizLbl = vgui.Create('DLabel')
	DBizLbl:SetParent(DCreateBiz)
	DBizLbl:Dock(TOP)
	DBizLbl:DockMargin(80, 5, 0, 0)
	DBizLbl:SetText('Business Name:')
	DBizLbl:SizeToContents()

	DBizTxt = vgui.Create('DTextEntry')
	DBizTxt:SetParent(DCreateBiz)
	DBizTxt:Dock(TOP)
	DBizTxt:DockMargin(80,5,80,0)
	DBizTxt:SetText('')

	DCreateBtn = vgui.Create('DButton')
	DCreateBtn:SetParent(DCreateBiz)
	DCreateBtn:Dock(BOTTOM)
	DCreateBtn:DockMargin(150, 10, 70, 10)
	DCreateBtn:SetText('Create')
	DCreateBtn.DoClick = function()
	net.Start("lpx_create_business")
	net.WriteString(DBizTxt:GetValue())
	net.WriteInt(tonumber(ManagerCut:GetValue()), 32)
	net.SendToServer()
	DCreateBiz:Close()
end

	ManagerCutInfo = vgui.Create('DLabel')
	ManagerCutInfo:SetParent(DCreateBiz)
	ManagerCutInfo:Dock(BOTTOM)
	ManagerCutInfo:DockMargin(65, 5,0, 0)
	ManagerCutInfo:SetText("(% of Employee's earnings that you get)")
	ManagerCutInfo:SizeToContents()

	ManagerCutLbl = vgui.Create('DLabel')
	ManagerCutLbl:SetParent(DCreateBiz)
	ManagerCutLbl:Dock(LEFT)
	ManagerCutLbl:DockMargin(80, 5, 0, 0)
	ManagerCutLbl:SetText('Manager Cut %:')
	ManagerCutLbl:SizeToContents()

	ManagerCut = vgui.Create('DNumberWang')
	ManagerCut:SetParent(DCreateBiz)
	ManagerCut:Dock(RIGHT)
	ManagerCut:DockMargin(0,5,80,0)
	ManagerCut:SetDecimals(0)
	ManagerCut:SetValue('10')
	ManagerCut:SetMinMax( 10, 40)
	ManagerCut:SizeToContents()
end
)

net.Receive("lpx_comp_req_fire", function(len, ply)
	local DEmployeeFire
	local DYesBtn
	local DNoBtn
	local DFireLbl

	local plyName = net.ReadString()
	DEmployeeFire = vgui.Create('DFrame')
	DEmployeeFire:SetSize(ScrW() * 0.20, ScrH() * 0.15)
	DEmployeeFire:Center()
	DEmployeeFire:SetTitle('Fire Employee')
	DEmployeeFire:SetSizable(true)
	DEmployeeFire:SetDeleteOnClose(false)
	DEmployeeFire:MakePopup()
	DEmployeeFire:SetBackgroundBlur(true)
	DEmployeeFire:ShowCloseButton(false)

	DFireLbl = vgui.Create("DLabel", DEmployeeFire)
	DFireLbl:Dock(TOP)
	DFireLbl:SetText("This computer is assigned to ".. plyName .. "\nWould you like to fire them?")

	DYesBtn = vgui.Create("DButton", DEmployeeFire)
	DYesBtn:Dock(LEFT)
	DYesBtn:SetText("Yes")
	DYesBtn.DoClick = function()
		net.Start("lpx_comp_fire")
		net.WriteString(plyName)
		net.SendToServer()
	end

	DNoBtn = vgui.Create("DButton", DEmployeeFire)
	DNoBtn:Dock(RIGHT)
	DNoBtn:SetText("No")
end
)

net.Receive("lpx_business_change_name", function(len, ply)
	local bizName = net.ReadString()
	chat.AddText(Color(0, 204, 204), "[".. bizName .."]", Color(255, 255, 255), " Business name changed from ", Color(0, 204, 204), net.ReadString(), Color(255, 255, 255), " to ", Color(0, 204, 204), bizName, Color(255, 255, 255), ".")
end
)

net.Receive("lpx_business_change_cut", function(len, ply)
	chat.AddText(Color(0, 204, 204), "[".. net.ReadString() .."]", Color(255, 255, 255), " Manager's Cut changed from ", Color(255, 255, 0), tostring(net.ReadInt(32)), "%", Color(255, 255, 255), " to ", Color(255, 255, 0), tostring(net.ReadInt(32)), "%", Color(255, 255, 255), ".")
end
)