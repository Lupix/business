-- network strings
util.AddNetworkString("lpx_comp_manage")
util.AddNetworkString("lpx_comp_manage_req_list")
util.AddNetworkString("lpx_comp_manage_list")
util.AddNetworkString("lpx_comp_assign")
util.AddNetworkString("lpx_comp_send_invite")
util.AddNetworkString("lpx_biz_invite")
util.AddNetworkString("lpx_comp_req_fire")
util.AddNetworkString("lpx_comp_fire")
util.AddNetworkString("lpx_profit_notify_emp")
util.AddNetworkString("lpx_profit_notify_mgr")
util.AddNetworkString("lpx_comp_use_notice")
util.AddNetworkString("lpx_comp_use_end_notice")

-- end of network strings

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")
include("autorun/server/lpx_init.lua")
include("autorun/server/lpx_business_cfg.lua")

-- network message handling
net.Receive("lpx_comp_manage_req_list", function(len, ply)
		local EligiblePlyName = {}

		for k, v in pairs(player.GetHumans()) do
			if v:Team() == TEAM_CITIZEN and not v.AssignedComputer then
				table.insert(EligiblePlyName, v:Nick())
			end
		end

		if !#EligiblePlyName then ply:PrintMessage(HUD_PRINTTALK , "No one is currently eligible to work for you.") return end
		net.Start("lpx_comp_manage_list")
		net.WriteTable(EligiblePlyName)
		net.Send(ply)
	end
)

net.Receive("lpx_comp_send_invite", function (len, ply)
	local target
	local targetName = net.ReadString()
	for k, v in pairs(player.GetHumans()) do
		if(v:Nick() == targetName) then
			target = v
		end
	end

	local tr = util.TraceLine( {
		start = ply:EyePos(),
		endpos = ply:EyePos() + ply:EyeAngles():Forward() * 500,
		filter = function( ent ) if ( ent:GetClass() == "lpx_computer" ) then return true end end
		} )

	local ent = tr.Entity

	local biz = GetBusinessByOwner(ply)
	target.ReqAssignEnt = ent
	net.Start("lpx_biz_invite")
	net.WriteString(ply:Nick())
	net.WriteString(biz["Name"])
	net.Send(target)
end
)

net.Receive("lpx_comp_fire", function(len, ply)
	local targetName = net.ReadString()
	local target
	local biz = GetBusinessByOwner(ply)
	for i=1, #(biz["Employees"]) do
		if biz["Employees"][i]:Nick() == targetName then
			target = biz["Employees"][i]
			table.remove(biz["Employees"], i)
		end
	end
	target.AssignedComputer.SetOccupied(false)
	target.AssignedComputer = nil 
	target.UsingComputer = false
	target:PrintMessage("You have been fired from " .. biz["Name"] .. ".")
	ply:PrintMessage("You have fired " .. targetName .. " from ".. biz["Name"] .. ".")
end
)

function ENT:SetupDataTables()
	self:NetworkVar("Bool", 0, "Occupied")
	self:NetworkVar("Entity", 0, "Manager")
end

function ENT:SpawnFunction(ply, tr, ClassName)
	if ( !tr.Hit ) then return end

	local SpawnPos = tr.HitPos + tr.HitNormal * 16

	local ent = ents.Create(ClassName)
	ent:SetPos(SpawnPos)
	ent:SetManager(ply)
	ent:Spawn()
	ent:Activate()

	return ent
end

net.Receive("lpx_comp_assign", function(len, ply)
	local ent = ply.ReqAssignEnt
	local mgr = ent:GetManager()
	print(ent:GetClass())
	AssignTo(ent, ply)
	local biz = GetBusinessByOwner(mgr)
	table.insert(biz["Employees"], ply)	
	ply:PrintMessage(HUD_PRINTTALK, "You are now an Employee of " .. biz["Name"] .. ".")
	mgr:PrintMessage(HUD_PRINTTALK, "You have successfully hired " .. ply:Nick() .. "!")
	ply.ReqAssignEnt = nil
end
)

function ENT:Initialize()

	self:SetModel("models/props_lab/workspace004.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)  
	self:SetMoveType(MOVETYPE_VPHYSICS)  
	self:SetSolid(SOLID_VPHYSICS)   
	self:SetUseType(SIMPLE_USE)    
end

function ENT:Use(activator, caller, useType, value)
	if(activator:IsPlayer()) then
		if activator:Team() == TEAM_BUSINESSMAN then
			if not self:GetOccupied() and self:GetManager() == activator then
				net.Start("lpx_comp_manage")
				net.Send(activator)
			elseif self:GetOccupied() and self:GetManager() == activator then
				local empName
				local biz = GetBusinessByOwner(activator)
				for i=1, #(biz["Employees"]) do
					if biz["Employees"][i].AssignedComputer == self then
						empName = biz["Employees"][i]:Nick()
					end
				end
				net.Start("lpx_comp_req_fire")
				net.WriteString(empName)
				net.Send()
			end
		elseif activator:Team() == TEAM_CITIZEN and activator.AssignedComputer == self and not activator.UsingComputer then
			activator.UsingComputer = true
			net.Start("lpx_comp_use_notice")
			net.WriteString(GetBusinessByOwner(ply.AssignedComputer:GetManager())["Name"])
			net.WriteString(activator:Nick())
			net.Send(self:GetManager())
			activator:GodEnable()
			activator:PrintMessage(HUD_PRINTTALK, "You are now using the Computer, type to earn money. Press SHIFT to stop.")
			activator:SetMoveType(MOVETYPE_NONE)
		end
	end
end
	
function AssignTo(ent, ply)
	if IsValid(ply) then
		if ply:Team() == TEAM_CITIZEN then
			ply.AssignedComputer = ent
			ent:SetOccupied(true)
		end
	end
end

local function CheckAssign(ply, before, after)
	if IsValid(ply) then
		if before == TEAM_CITIZEN and ply.AssignedComputer then
			if IsValid(ply.AssignedComputer) then ply.AssignedComputer:SetOccupied(false) end
			ply.AssignedComputer = nil
		end
	end
end

hook.Add("OnPlayerChangedTeam", "PlayerChangeTeam", CheckAssign)
hook.Add("PlayerDisconnected", "PlayerDisconnect", function(ply)
		if IsValid(ply.AssignedComputer) then ply.AssignedComputer:SetOccupied(false) end
	end
)

local function PlayerPressKey(ply, key)

	if not ply.LastPress then ply.LastPress = RealTime() end
	if ply.UsingComputer and RealTime() - ply.LastPress > 0.200 then
		if(key == IN_SPEED) then
			ply.UsingComputer = false
			net.Start("lpx_comp_use_end_notice")
			net.WriteString(GetBusinessByOwner(ply.AssignedComputer:GetManager())["Name"])
			net.WriteString(ply:Nick())
			net.Send(ply.AssignedComputer:GetManager())
			ply:GodDisable()
			ply:SetMoveType(MOVETYPE_WALK)
		end
		if not ply.KeyStrokes then ply.KeyStrokes = 1 end
		ply.KeyStrokes = ply.KeyStrokes + 1
		if not ply.EarnedThisMin then ply.EarnedThisMin = 0 end
		if not ply.EarnedForMgr then ply.EarnedForMgr = 0 end
		if ply.KeyStrokes > LPX_EMPLOYEE_KEY_AMOUNT then
			local mgr = ply.AssignedComputer:GetManager()
			if IsValid(mgr) then
				local biz = GetBusinessByOwner(mgr)
				local mgrCut = math.ceil((LPX_EMPLOYEE_PROFIT_BASE / 100) * biz["MgrCut"])
				mgr:addMoney(mgrCut)
				ply:addMoney(LPX_EMPLOYEE_PROFIT_BASE - mgrCut)
				ply.KeyStrokes = 0
				ply.EarnedThisMin = ply.EarnedThisMin + (LPX_EMPLOYEE_PROFIT_BASE - mgrCut)
				ply.EarnedForMgr = ply.EarnedForMgr + mgrCut
				if not timer.Exists("timer_profit_notify_" .. ply:UniqueID()) then
					timer.Create("timer_profit_notify_".. ply:UniqueID(), 60, 0, function() 
						if not ply.UsingComputer then timer.Destroy("timer_profit_notify_".. ply:UniqueID()) return end
						net.Start("lpx_profit_notify_emp")
						net.WriteString(biz["Name"])
						net.WriteInt(ply.EarnedThisMin, 32)
						net.Send(ply)
						net.Start("lpx_profit_notify_mgr")
						net.WriteString(biz["Name"])
						net.WriteString(ply:Nick())
						net.WriteInt(ply.EarnedForMgr, 32)
						net.Send(mgr)
						ply.EarnedThisMin = 0
						ply.EarnedForMgr = 0
					end
					)
				end
			end
		end
		ply.LastPress = RealTime()
	end
end

hook.Add("KeyPress","PlayerPressKey", PlayerPressKey)
