include("shared.lua")

net.Receive("lpx_comp_manage_list", function(len, ply)

	local DHireBtn
	local DPlayerList
	local DContainer

	local plyNames = net.ReadTable()

	DContainer = vgui.Create('DFrame')
	DContainer:SetSize(200, 100)
	DContainer:Center()
	DContainer:SetTitle('Hire Employee')
	DContainer:SetSizable(true)
	DContainer:SetDeleteOnClose(true)
	DContainer:MakePopup()

	DHireBtn = vgui.Create('DButton')
	DHireBtn:SetParent(DContainer)
	DHireBtn:Dock(BOTTOM)
	DHireBtn:SetText('Hire')
	DHireBtn.DoClick = function() 
		local targetName = DPlayerList:GetSelected()
		if targetName then
			net.Start("lpx_comp_send_invite")
			net.WriteString(targetName)
			net.SendToServer()
			DContainer:Close()
		end
	end

	DPlayerList = vgui.Create('DComboBox')
	DPlayerList:SetParent(DContainer)
	DPlayerList:SetValue("Eligible Players")
	DPlayerList:Dock(TOP)
	DPlayerList:DockMargin(0,0,0,0)
	for i=1, #plyNames do DPlayerList:AddChoice(plyNames[i]) end
end
)

net.Receive("lpx_comp_manage", function(len, ply)
	local DHireLbl
	local DHireBtn
	local DEmployeeHire

	DEmployeeHire = vgui.Create('DFrame')
	DEmployeeHire:SetSize(264, 101)
	DEmployeeHire:Center()
	DEmployeeHire:SetTitle('Hire Employee')
	DEmployeeHire:SetDeleteOnClose(false)
	DEmployeeHire:MakePopup()

	DHireLbl = vgui.Create('DLabel')
	DHireLbl:SetParent(DEmployeeHire)
	DHireLbl:Dock(TOP)
	DHireLbl:SetText('You must hire someone to assign this computer to.')
	DHireLbl:SizeToContents()

	DHireBtn = vgui.Create('DButton')
	DHireBtn:SetParent(DEmployeeHire)
	DHireBtn:Dock(BOTTOM)
	DHireBtn:SetText('Hire')
	DHireBtn.DoClick = function() 
		net.Start("lpx_comp_manage_req_list")
		net.SendToServer()
		DEmployeeHire:Close()
	end

end
)

net.Receive("lpx_biz_invite", function(len, ply)
	local DInvLbl
	local DAcceptBtn
	local DDeclineBtn
	local DInvite

	DInvite = vgui.Create("DFrame")
	DInvite:SetSize(250, 100)
	DInvite:Center()
	DInvite:SetTitle("Business Invite")

	DInvLbl = vgui.Create("DLabel", DInvite)
	DInvLbl:Dock(TOP)
	DInvLbl:SetText("You have been invited by " .. net.ReadString() .. " to join " .. net.ReadString())
	DInvLbl:SizeToContents()

	DAcceptBtn = vgui.Create("DButton", DInvite)
	DAcceptBtn:Dock(LEFT)
	DAcceptBtn:SetText("Accept")
	DAcceptBtn.DoClick = function(btn)
		net.Start("lpx_comp_assign")
		net.SendToServer()
		DInvite:Close()
	end

	DDeclineBtn = vgui.Create("DButton", DInvite)
	DDeclineBtn:Dock(RIGHT)
	DDeclineBtn:SetText("Decline")
	DDeclineBtn.DoClick = function(btn)
		DInvite:Close()
	end
end
)

net.Receive("lpx_profit_notify_emp", function(len, ply)
	chat.AddText(Color(0, 204, 204), "[".. net.ReadString() .."]", Color(255, 255, 255), " You have gained ", Color(102, 255, 102), "$", tostring(net.ReadInt(32)), Color(255, 255, 255), " in the last minute.")
end
)

net.Receive("lpx_profit_notify_mgr", function(len, ply)
	chat.AddText(Color(0, 204, 204), "[".. net.ReadString() .."]", Color(255, 255, 0), net.ReadString(), Color(255, 255, 255), " earned you ", Color(102, 155, 102), "$", tostring(net.ReadInt(32)), Color(255, 255, 255), " in the last minute.")
end
)

net.Receive("lpx_comp_use_notice", function(len, ply)
	chat.AddText(Color(0, 204, 204), "[".. net.ReadString() .."] ", Color(255, 255, 0), net.ReadString(), Color(255, 255, 255), " has begun working on the computer.")
end
)

net.Receive("lpx_comp_use_end_notice", function(len, ply)
	chat.AddText(Color(0, 204, 204), "[".. net.ReadString() .."] ", Color(255, 255, 0), net.ReadString(), Color(255, 255, 255), " has stopped working on the computer.")
end
)