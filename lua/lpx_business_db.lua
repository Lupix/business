function TableInit()
	if not sql.TableExists("lpx_business") then

		sql.Query("CREATE TABLE lpx_business (Owner_UID varchar(255), Biz_Name varchar(100), Mgr_Cut int)")

		if sql.TableExists("lpx_business") then
			Msg("[LPX_BUSINESS] Data tables successfully created")
		else
			Msg("[LPX_BUSINESS] Could not create data tables. Check error below:")
			Msg("[LPX_BUSINESS] [ERROR] " .. sql.LastError())
		end
	end
end

function LoadBusinessByOwner(owner)
	local res = sql.QueryRow("SELECT * FROM lpx_business WHERE Owner_UID='" .. owner:SteamID64() .. "'")
	if not res then return false
	else
			local Business = {Name = res["Biz_Name"], MgrCut = tonumber(res["Mgr_Cut"]), Owner=player.GetBySteamID64(res["Owner_UID"]), Employees = {}}
			table.insert(Businesses, Business)
			return true
	end
end

function SaveBusiness(business)
	local res = sql.Query("SELECT * FROM lpx_business WHERE Biz_Name='".. business["Name"] .. "'")
	if res then return false end
	res = nil
	res = sql.Query("SELECT * FROM lpx_business WHERE Owner_UID='" .. business["Owner"]:SteamID64() .. "'")
	print("Saving Business: ", "Name: " .. business["Name"], "Owner: " .. business["Owner"]:Nick(), "Manager Cut: " .. business["MgrCut"] .. "|")
	if not res or table.Count(res) == 0 then
		sql.Query("INSERT INTO lpx_business VALUES ('".. business["Owner"]:SteamID64() .."', '".. business["Name"] .."', ".. business["MgrCut"]..")")
	elseif table.Count(res) > 0 then
		sql.Query("UPDATE lpx_business SET Biz_Name='".. business["Name"] .."', Mgr_Cut=".. business["MgrCut"] .. " WHERE Owner_UID='".. business["Owner"]:SteamID64() .. "'")
	end
	return true
end